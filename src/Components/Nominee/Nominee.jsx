import "./nominee.css";

function Nominee({
  nomineeId,
  name,
  imgUrl,
  selectedNomineeIDs,
  onNomineeClick,
}) {
  return (
    <div
      className={`nominee ${
        selectedNomineeIDs.includes(nomineeId) ? "nominee-selected" : ""
      }`}
      title={name}
    >
      <span className="nominee-name">{name}</span>
      <img src={imgUrl} className="nominee-photo" alt={`Poster of ${name}`} />
      <button className="nominee-button" onClick={onNomineeClick}>
        Select
      </button>
    </div>
  );
}

export default Nominee;
