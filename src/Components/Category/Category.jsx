import "./category.css";
import Nominee from "../Nominee/Nominee";

function Category({
  categoryId,
  categoryName,
  nomineeList,
  selectedNomineeIDs,
  onNomineeSelected,
}) {
  return (
    <div className="category">
      <div className="category-title">{categoryName}</div>
      <div className="category-movies">
        {nomineeList.map((nominee) => (
          <Nominee
            key={nominee.id}
            nomineeId={nominee.id}
            name={nominee.title}
            imgUrl={nominee.photoUrL}
            selectedNomineeIDs={selectedNomineeIDs}
            onNomineeClick={() => onNomineeSelected(categoryId, nominee)}
          />
        ))}
      </div>
    </div>
  );
}

export default Category;
