import "./submit-nav.css";

function SubmitNav({ onBallotSubmit }) {
  return (
    <div className="submitnav">
      <div className="submitnav-content">
        <button onClick={onBallotSubmit} className="submitnav-button">
          Submit
        </button>
      </div>
    </div>
  );
}

export default SubmitNav;
