import "./modal.css";
import closeIcon from "./close.png";

function Modal({ showModal, nomineeList, selectedNominees, onCloseModal }) {
  return (
    <div className={`modal-bg ${showModal ? "display-block" : "display-none"}`}>
      <div className="modal">
        <img
          className="modal-button--close"
          src={closeIcon}
          alt="Close Icon"
          onClick={onCloseModal}
        />
        <h2 className="modal-title">Selected Nominee</h2>
        <table id="modal-table">
          <thead>
            <tr>
              <th>Category</th>
              <th>Nominee</th>
            </tr>
          </thead>
          <tbody>
            {nomineeList.map((nominee) => (
              <tr key={nominee.id}>
                <td>{nominee.title}</td>
                <td>
                  {
                    selectedNominees.find(
                      (data) => data.categoryId === nominee.id
                    )?.nominee?.title
                  }
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Modal;
