import { useState, useEffect } from "react";
import Category from "./Components/Category/Category";
import SubmitNav from "./Components/SubmitNav/SubmitNav";
import Modal from "./Components/Modal/Modal";
import Api from "./Api/Api";
import "./Styles/App.css";

function App() {
  const [nominees, setNominees] = useState([]);
  const [selectedNominees, setSelectedNominees] = useState([]);
  const [selectedNomineeIDs, setSelectedNomineeIDs] = useState([]);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    Api.getBallotData()
      .then((res) => setNominees(res.items))
      .catch(console.log);
  }, []);

  useEffect(() => {
    setSelectedNomineeIDs(selectedNominees.map((data) => data.nominee.id));
  }, [selectedNominees]);

  const handleNomineeSelected = (categoryId, nominee) => {
    const filteredNominees = selectedNominees.filter(
      (nominee) => nominee.categoryId !== categoryId
    );
    filteredNominees.push({
      categoryId,
      nominee,
    });
    setSelectedNominees(filteredNominees);
  };

  return (
    <div>
      <div className="app">
        <h2 className="app-title">AWARDS 2021</h2>
        {nominees.map((nominee) => (
          <Category
            key={nominee.id}
            categoryId={nominee.id}
            categoryName={nominee.title}
            nomineeList={nominee.items}
            selectedNomineeIDs={selectedNomineeIDs}
            onNomineeSelected={handleNomineeSelected}
          />
        ))}
      </div>
      {nominees.length > 0 ? (
        <SubmitNav onBallotSubmit={() => setShowModal(true)} />
      ) : null}
      <Modal
        showModal={showModal}
        nomineeList={nominees}
        selectedNominees={selectedNominees}
        onCloseModal={() => setShowModal(false)}
      />
    </div>
  );
}

export default App;
